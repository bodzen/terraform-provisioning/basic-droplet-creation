resource "digitalocean_droplet" "web" {
	image  = "ubuntu-18-04-x64"
	name   = "${var.DO_SPACE_NAME}"
	region = "${var.DO_SPACE_REGION}"
	size   = "s-1vcpu-1gb"
}
