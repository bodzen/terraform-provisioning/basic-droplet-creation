terraform init -input=false
terraform validate
terraform plan -out=tf_plan -input=false
terraform apply -input=false tf_plan
