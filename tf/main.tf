provider "digitalocean" {
	token = "${var.DO_TOKEN}"
	version = "~> 1.8.0"
}
